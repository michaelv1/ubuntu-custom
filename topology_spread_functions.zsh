pods_az_spread() {
  # Fetch all pods and their associated nodes
  local pod_filter="$*"
  local pods_and_nodes
  local regex

  if [[ -z "$pod_filter" ]]; then
    # No filter, get all pods
    pods_and_nodes=$(kubectl get pods -o=jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.spec.nodeName}{"\n"}{end}')
  else
    # Convert multiple parameters into a regex pattern
    regex=$(echo "$pod_filter" | sed 's/ /|/g') # Replace spaces with `|` for OR pattern
    # Filter by pod name prefix
    pods_and_nodes=$(kubectl get pods -o=jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.spec.nodeName}{"\n"}{end}' | grep -E "^($regex)")
  fi

  # Display header for pod details
  printf "\n%-60s %-30s %-15s\n" "POD NAME" "NODE NAME" "ZONE"
  printf "%-60s %-30s %-15s\n" "----------------------------------------------------------" "-----------------------------" "---------------"

  # Create an array to hold pod details
  local pods_list=()
  local pod_details=()

  # Loop through pods and collect data
  while read -r pod node; do
    if [[ -n "$node" ]]; then
      # Fetch zone
      az=$(kubectl get node "$node" -o=jsonpath='{.metadata.labels.topology\.kubernetes\.io/zone}' 2>/dev/null)
    else
      az="<N/A>"
    fi

    # Extract deployment name
    local deployment_name="${pod%-*}"      # Remove last segment
    deployment_name="${deployment_name%-*}" # Remove second-last segment

    # Add pod details to the list
    pod_details+=("$pod|$node|$az")
    pods_list+=("$deployment_name|$az")
  done <<< "$pods_and_nodes"

  # Print the pod details
  for detail in "${pod_details[@]}"; do
    local pod_name="${detail%%|*}"
    local remaining="${detail#*|}"
    local node_name="${remaining%%|*}"
    local az="${remaining##*|}"
    printf "%-60s %-30s %-15s\n" "$pod_name" "$node_name" "$az"
  done

  # Aggregate by deployment and zone
  local -A aggregation
  for item in "${pods_list[@]}"; do
    ((aggregation["$item"]++))
  done

  # Display aggregated spread by deployment and AZ
  echo
  echo "Aggregated Spread by Deployment and AZ:"
  printf "%-30s %-15s %-10s\n" "DEPLOYMENT" "ZONE" "POD COUNT"
  printf "%-30s %-15s %-10s\n" "-----------------------------" "---------------" "---------"

  # Sort and print the results
  for key in ${(on)${(k)aggregation}}; do
    local deployment_name="${key%%|*}"
    local az="${key##*|}"
    printf "%-30s %-15s %-10d\n" "$deployment_name" "$az" "${aggregation[$key]}"
  done
}
opensearch_az_spread() {
  # Fetch all OpenSearch domains in the account as a list
  local domains
  domains=$(aws opensearch list-domain-names --query "DomainNames[*].DomainName" --output json | jq -r '.[]')

  # Check if the domains list is empty
  if [[ -z "$domains" ]]; then
    echo "No OpenSearch domains found in the account."
    return 1
  fi

  # Display header
  printf "%-30s %-50s\n" "DOMAIN NAME" "AVAILABILITY ZONES"
  printf "%-30s %-50s\n" "------------------------------" "--------------------------------------------------"

  # Loop through each domain and fetch its AZs
  while read -r domain; do
    # Fetch subnet IDs for the domain
    local subnet_ids
    subnet_ids=$(aws opensearch describe-domain --domain-name "$domain" --query "DomainStatus.VPCOptions.SubnetIds" --output json | jq -r '.[]' 2>/dev/null)

    if [[ -n "$subnet_ids" ]]; then
      # Extract AZs for the subnets
      local azs
      azs=$(aws ec2 describe-subnets --subnet-ids $subnet_ids --query "Subnets[*].AvailabilityZone" --output text 2>/dev/null | tr '\n' ',' | sed 's/,$//')

      if [[ -n "$azs" ]]; then
        # Print only the domain name and AZs
        printf "%-30s %-50s\n" "$domain" "$azs"
      else
        printf "%-30s %-50s\n" "$domain" "<Unable to Retrieve AZs>"
      fi
    else
      printf "%-30s %-50s\n" "$domain" "<No Subnets Configured>"
    fi
  done <<< "$domains"
}