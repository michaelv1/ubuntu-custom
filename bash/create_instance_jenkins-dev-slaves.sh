#!/bin/bash

# Set the necessary variables
instance_type="r6a.8xlarge"  # Change this to the desired instance type
key_name="devops"  # Change this to your key pair name
security_group_id="sg-e5320897"  # Change this to your security group ID
subnet_id="subnet-de29a995"  # Change this to your subnet ID
iam_role_name="dev-jenkins"  # Change this to your IAM role name
tag_key="Name"
tag_value="jenkins-pcn-dev-slaves-test"

# Create the EC2 instance
aws ec2 run-instances \
    --image-id ami-0d3eb1f646fbce3ad \
    --instance-type $instance_type \
    --key-name $key_name \
    --security-group-ids $security_group_id \
    --subnet-id $subnet_id \
    --iam-instance-profile Name=$iam_role_name \
    --tag-specifications "ResourceType=instance,Tags=[{Key=$tag_key,Value=$tag_value}]"    

## register to ALB = Jenkins-ALB-DEV-QA