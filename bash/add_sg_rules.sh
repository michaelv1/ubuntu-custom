#!/bin/bash

# Set variables
SECURITY_GROUP_ID="sg-07df3f66e27a2ca97"
CIDR_BLOCKS="10.80.0.0/16,10.81.0.0/16,10.82.0.0/16,10.83.0.0/16"

# Add ingress rule to security group
IFS=',' read -ra CIDRS <<< "$CIDR_BLOCKS"
for cidr in "${CIDRS[@]}"; do
    aws ec2 authorize-security-group-ingress \
        --group-id "$SECURITY_GROUP_ID" \
        --ip-permissions IpProtocol=tcp,FromPort=443,ToPort=443,IpRanges='[{CidrIp='"$cidr"',Description="HTTPS access"}]' \
        --output text
done
