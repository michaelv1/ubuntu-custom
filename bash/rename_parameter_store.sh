#!/bin/bash

while IFS= read -r line; do
  OLD_PARAMETER_NAME="$line"
  NEW_PARAMETER_NAME=$(echo "$line" | sed 's|/jenkins/devops-test/|/jenkins/jenkins-test/|')
  OLD_PARAMETER_VALUE=$(aws ssm get-parameter --name "$OLD_PARAMETER_NAME" --query "Parameter.Value" --output text)
  aws ssm put-parameter --name "$NEW_PARAMETER_NAME" --value "$OLD_PARAMETER_VALUE" --type "String" --overwrite
done <<END
/jenkins/devops-test/kubernetes-devops-eks/endpoint
END
