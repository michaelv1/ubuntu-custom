#!/bin/bash

# Get the list of PVCs
pvc_list=$(kubectl get pvc | grep -iE "nifi-0|nifi-1|nifi-2" | awk '{print $1}')

# Iterate over each PVC and delete it
while IFS= read -r pvc_name; do
    echo "Deleting PVC: $pvc_name"
    kubectl delete pvc "$pvc_name"
done <<< "$pvc_list"
