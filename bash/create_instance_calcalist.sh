#!/bin/bash

# Set the necessary variables
instance_type="c5d.2xlarge"  # Change this to the desired instance type
key_name="devops"  # Change this to your key pair name
security_group_id="sg-b2921dc0"  # Change this to your security group ID
subnet_id="subnet-fcedf7d0"  # Change this to your subnet ID
iam_role_name="dev-jenkins"  # Change this to your IAM role name
tag_key="Name"
tag_value="calcalist"

# Create the EC2 instance
instance_id=$(aws ec2 run-instances \
    --image-id ami-07d5c5bc2f198d08b \
    --instance-type $instance_type \
    --key-name $key_name \
    --security-group-ids $security_group_id \
    --subnet-id $subnet_id \
    --iam-instance-profile Name=$iam_role_name \
    --tag-specifications "ResourceType=instance,Tags=[{Key=$tag_key,Value=$tag_value}]" \
    --query 'Instances[0].InstanceId' \
    --output text)

# Allocate an Elastic IP
allocation_id=$(aws ec2 allocate-address \
    --domain vpc \
    --query 'AllocationId' \
    --output text)

# Associate the Elastic IP with the instance
aws ec2 associate-address \
    --instance-id $instance_id \
    --allocation-id $allocation_id
