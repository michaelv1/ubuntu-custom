#!/bin/bash

namespace=$1
secret_name=$2
key=$3
value=$4

# Check if all variables are provided
if [ -z "$namespace" ] || [ -z "$secret_name" ] || [ -z "$key" ] || [ -z "$value" ]; then
  echo "Error: Missing one or more variables."
  echo "Usage: ./script.sh <namespace> <secret_name> <key> <value>"
  exit 1
fi

namespace=${namespace}
secret_name=${secret_name}
file_path="file.yaml"
key=${key}
value=${value}

# Encode the value
encoded_value=$(echo -n "$value" | base64)

# Create the YAML file
file_content="
apiVersion: v1
kind: Secret
metadata:
  name: $secret_name
type: Opaque
data:
  $key: $encoded_value
"

echo "$file_content" > "$file_path"

# Apply the file using kubectl
kubectl apply -f "$file_path"
