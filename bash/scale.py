# Import the required Kubernetes client libraries
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import time
import sys

# Load the Kubernetes configuration
config.load_kube_config()

# Initialize the Kubernetes API client
api_instance = client.AppsV1Api()
api_core = client.CoreV1Api()

# Define the function to scale the StatefulSet to 0
def scale_statefulset_to_zero(statefulset_name, namespace):
    try:
        # Get the current StatefulSet object
        statefulset = api_instance.read_namespaced_stateful_set(name=statefulset_name, namespace=namespace)

        # Store the current replica count
        replica_count = statefulset.spec.replicas

        # Set the replica count to 0
        statefulset.spec.replicas = 0

        # Update the StatefulSet object
        api_instance.patch_namespaced_stateful_set(name=statefulset_name, namespace=namespace, body=statefulset)

        print(f"StatefulSet '{statefulset_name}' scaled to 0 replicas.")

        # Return the original replica count
        return replica_count

    except ApiException as e:
        print(f"Exception when scaling StatefulSet: {e}")

# Define the function to wait until all pods are terminated
def wait_for_pods_termination(statefulset_name, namespace):
    while True:
        try:
            # Get the list of pods for the StatefulSet
            pods = api_core.list_namespaced_pod(namespace=namespace, label_selector=f"statefulset.kubernetes.io/pod-name={statefulset_name}")

            # Check if all pods are terminated
            if len(pods.items) == 0:
                break  # All pods have been terminated

        except ApiException as e:
            print(f"Exception when getting pods: {e}")

        time.sleep(5)  # Wait for 5 seconds before checking again

    print("All pods have been terminated.")

# Define the function to scale the StatefulSet back up
def scale_statefulset_up(statefulset_name, namespace, replica_count):
    try:
        # Get the current StatefulSet object
        statefulset = api_instance.read_namespaced_stateful_set(name=statefulset_name, namespace=namespace)

        # Set the replica count to the original value
        statefulset.spec.replicas = replica_count

        # Update the StatefulSet object
        api_instance.patch_namespaced_stateful_set(name=statefulset_name, namespace=namespace, body=statefulset)

        print(f"StatefulSet '{statefulset_name}' scaled up to {replica_count} replicas.")

    except ApiException as e:
        print(f"Exception when scaling StatefulSet: {e}")

# Check if the StatefulSet name and namespace are provided as command-line arguments
if len(sys.argv) != 3:
    print("Usage: python statefulset_operator.py <statefulset_name> <namespace>")
    sys.exit(1)

# Get the StatefulSet name and namespace from the command-line arguments
statefulset_name = sys.argv[1]
namespace = sys.argv[2]

# Scale the StatefulSet to 0 and get the original replica count
original_replica_count = scale_statefulset_to_zero(statefulset_name, namespace)

# Wait until all pods are terminated
wait_for_pods_termination(statefulset_name, namespace)

# Scale the StatefulSet back up using the original replica count
scale_statefulset_up(statefulset_name, namespace, original_replica_count)
