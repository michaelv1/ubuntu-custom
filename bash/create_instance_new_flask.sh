#!/bin/bash

# Set the necessary variables
image_id="ami-022e1a32d3f742bd8"
instance_type="t2.medium"  # Change this to the desired instance type
key_name="prod"  # Change this to your key pair name
security_group_id="sg-08f9ef501b774fdaa"  # Change this to your security group ID
subnet_id="subnet-0f84cf3e2be711cb7"  # Change this to your subnet ID
iam_role_name="Downloader"  # Change this to your IAM role name
tag_key="Name"
tag_value="flask"

# Create the EC2 instance
aws ec2 run-instances \
    --image-id $image_id \
    --instance-type $instance_type \
    --key-name $key_name \
    --security-group-ids $security_group_id \
    --subnet-id $subnet_id \
    --iam-instance-profile Name=$iam_role_name \
    --tag-specifications "ResourceType=instance,Tags=[{Key=$tag_key,Value=$tag_value}]"    

## register to ALB = Jenkins-ALB-DEV-QA