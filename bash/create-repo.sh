#!/bin/bash

# Check if repository name parameter is provided
if [ -z "$1" ]; then
    echo "Please provide a repository name."
    exit 1
fi

# Set the variable for the repository name
repository_name="$1"

# Create the ECR repository
aws ecr create-repository --repository-name "$repository_name"

# Set the repository policy
policy='{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "AllowPushPull",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::252822179119:root",
          "arn:aws:iam::056612754658:root",
          "arn:aws:iam::314823803723:root"
        ]
      },
      "Action": "ecr:*"
    }
  ]
}'

aws ecr set-repository-policy --repository-name "$repository_name" --policy-text "$policy"

