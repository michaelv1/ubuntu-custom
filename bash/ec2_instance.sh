#!/bin/bash

# Set the necessary variables
instance_type="t2.medium"  # Change this to the desired instance type
key_name="prod"  # Change this to your key pair name
subnet_id="subnet-0f84cf3e2be711cb7"  # Change this to your subnet ID
iam_role_name="ac-prod-flask-instance-profile"  # Change this to your IAM role name
tag_key="Name"
tag_value="michael-test"

# Create the EC2 instance
instance_id=$(aws ec2 run-instances \
    --image-id ami-022e1a32d3f742bd8 \
    --instance-type $instance_type \
    --key-name $key_name \
    --security-group-ids sg-08f9ef501b774fdaa  sg-0207abda7eacb3972 \
    --subnet-id $subnet_id \
    --iam-instance-profile Name=$iam_role_name \
    --tag-specifications "ResourceType=instance,Tags=[{Key=$tag_key,Value=$tag_value}]" \
    --query 'Instances[0].InstanceId' \
    --output text)
