#!/bin/bash

# JENKINS_CIDR="10.80.0.0/16,10.81.0.0/16,10.82.0.0/16,10.83.0.0/16"

# DEV="10.110.0.0/16,10.111.0.0/16,10.112.0.0/16,10.113.0.0/16"
# QA="10.120.0.0/16,10.121.0.0/16,10.122.0.0/16,10.123.0.0/16"
# STG="10.70.0.0/16,10.71.0.0/16,10.72.0.0/16,10.73.0.0/16"
# PROD="10.60.0.0/16,10.61.0.0/16,10.62.0.0/16,10.63.0.0/16"
# EUPROD="10.201.0.0/16,10.201.0.0/16,10.202.0.0/16,10.203.0.0/16"

# EUPROD_TGW="tgw-0800b97b833d71b71"
# SHARED_TGW="tgw-0e696f0cec5b99941"

# JENKINS_ROUTE_TABLE_IDS="rtb-04a0b2265c05fddf6,rtb-06a7fea8f1a8b2c21,rtb-017c367527a36d163"

# EUPRO_ROUTE_TABLE_IDS="rtb-0ff5a95c9f7cd3552,rtb-0a8ee713c604a7e5e,rtb-07d7065552288632b"
# QA_ROUTE_TABLE_IDS="rtb-07012f1ed2117c624,rtb-07e34d830df8b07d6,rtb-07bced67e5d3a3582"
# STG__ROUTE_TABLE_IDS="rtb-05d3ce7d17b8cae6f,rtb-09aae216ab7455b42,rtb-02b20228be6d06e91"

# Set variables
ROUTE_TABLE_IDS="rtb-04a0b2265c05fddf6,rtb-06a7fea8f1a8b2c21,rtb-017c367527a36d163"
DESTINATION_CIDR_BLOCKS="10.60.0.0/16,10.61.0.0/16,10.62.0.0/16,10.63.0.0/16"
GATEWAY_ID="tgw-0e696f0cec5b99941"

# Add routes to routing tables
IFS=',' read -ra TABLE_IDS <<< "$ROUTE_TABLE_IDS"
for rtb_id in "${TABLE_IDS[@]}"; do
    IFS=',' read -ra CIDR_BLOCKS <<< "$DESTINATION_CIDR_BLOCKS"
    for cidr_block in "${CIDR_BLOCKS[@]}"; do
        aws ec2 create-route \
            --route-table-id "$rtb_id" \
            --destination-cidr-block "$cidr_block" \
            --gateway-id "$GATEWAY_ID"
    done
done
