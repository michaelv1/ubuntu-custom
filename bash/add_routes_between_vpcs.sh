#!/bin/bash

JENKINS_VPC="vpc-07f83eef842bab5fa"
JENKINS_ACCOUNT="default"
DESTINATION_VPC="vpc-02691d97f063d6814"
DESTINATION_ACCOUNT="default"
DESTINATION_ADMIN_SG="sg-0cadb72561eab60f3"
TRANSIT_GATEWAY="tgw-0e696f0cec5b99941"

# Get the CIDR blocks for both VPCs
JENKINS_CIDRS=$(aws ec2 describe-vpcs --vpc-ids "${JENKINS_VPC}" | jq -r '.Vpcs[0].CidrBlockAssociationSet[].CidrBlock' | tr '\n' ',' | sed 's/,$//')
DESTINATION_CIDRS=$(aws ec2 describe-vpcs --vpc-ids "${DESTINATION_VPC}" | jq -r '.Vpcs[0].CidrBlockAssociationSet[].CidrBlock' | tr '\n' ',' | sed 's/,$//')

# Get the RouteTable IDs for both VPCs
JENKINS_ROUTE_TABLES=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${JENKINS_VPC}" | jq -r '.RouteTables[].RouteTableId' | paste -sd ',')
DESTINATION_ROUTE_TABLES=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=${DESTINATION_VPC}" | jq -r '.RouteTables[].RouteTableId' | paste -sd ',')

echo "JENKINS_ROUTE_TABLES: ${JENKINS_ROUTE_TABLES}"
echo "JENKINS_CIDRS: ${JENKINS_CIDRS}"
echo "DESTINATION_ROUTE_TABLES: ${DESTINATION_ROUTE_TABLES}"
echo "DESTINATION_CIDRS: ${DESTINATION_CIDRS}"

# # Add routes in JENKINS_ROUTE_TABLES to DESTINATION_CIDRS using the TRANSIT_GATEWAY
# for route_table_id in $(echo "${JENKINS_ROUTE_TABLES}" | tr ',' ' '); do
#   for cidr in $(echo "${DESTINATION_CIDRS}" | tr ',' ' '); do
#     aws ec2 create-route --route-table-id "${route_table_id}" --destination-cidr-block "${cidr}" --gateway-id "${TRANSIT_GATEWAY}" > /dev/null
#   done
# done

# # Add routes in DESTINATION_ROUTE_TABLES to JENKINS_CIDRS using the TRANSIT_GATEWAY
# for route_table_id in $(echo "${DESTINATION_ROUTE_TABLES}" | tr ',' ' '); do
#   for cidr in $(echo "${JENKINS_CIDRS}" | tr ',' ' '); do
#     aws ec2 create-route --route-table-id "${route_table_id}" --destination-cidr-block "${cidr}" --gateway-id "${TRANSIT_GATEWAY}" > /dev/null
#   done
# done

# Add rules to DESTINATION_ADMIN_SG to allow access from all CIDRs in JENKINS_CIDRS
for cidr in $(echo "${JENKINS_CIDRS}" | tr ',' ' '); do
  aws ec2 authorize-security-group-ingress --group-id "${DESTINATION_ADMIN_SG}" --protocol tcp --port 443 --cidr "${cidr}" > /dev/null
done