#!/bin/bash

# Set the IP address for which you want to find the subnet
IP_ADDRESS="10.92.7.70"

echo ${IP_ADDRESS}

# Run the describe-subnets command to retrieve all subnets
SUBNETS=$(aws ec2 describe-subnets --query 'Subnets[*].{ID:SubnetId,CIDR:CidrBlock}' --output json)

# Iterate through the subnets to find the matching subnet
for row in $(echo "${SUBNETS}" | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }
    SUBNET_CIDR=$(_jq '.CIDR')
    SUBNET_ID=$(_jq '.ID')

    # Check if the IP address falls within the subnet CIDR range
    if ipcalc --network "${SUBNET_CIDR}" | grep -q "${IP_ADDRESS}"; then
        # Print the subnet ID
        echo "Subnet ID: ${SUBNET_ID}"

        # Find the associated route tables
        ROUTE_TABLES=$(aws ec2 describe-route-tables --filters "Name=association.subnet-id,Values=${SUBNET_ID}" --query 'RouteTables[*].RouteTableId' --output text)

        # Print the associated route tables
        echo "Associated Route Tables:"
        echo "${ROUTE_TABLES}"

        # Iterate through the associated route tables
        while IFS= read -r route_table_id; do
            echo "Routes for Route Table: ${route_table_id}"

            # Retrieve the routes for the route table
            ROUTES=$(aws ec2 describe-route-tables --route-table-ids "${route_table_id}" --query 'RouteTables[].Routes[]' --output json)

            # Parse and print the destinations and targets
            echo "${ROUTES}" | jq -r '.[] | "Destination: \(.DestinationCidrBlock), Target: \(.GatewayId//.NatGatewayId//.VpcPeeringConnectionId//.TransitGatewayId//.EgressOnlyInternetGatewayId//.InstanceOwnerId//.NetworkInterfaceId)"'

            echo
        done <<< "${ROUTE_TABLES}"

        exit 0
    fi
done

# If no matching subnet is found, print a message
echo "No subnet found for IP address: ${IP_ADDRESS}"
