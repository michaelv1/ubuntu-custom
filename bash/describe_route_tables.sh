DEV_VPC_ID="vpc-08d99de73b4674313"
QA_VPC_ID="vpc-0015a47bdc3e08f3e"
STG_VPC_ID="vpc-0931318ae766bff8b"

aws ec2 describe-route-tables \
    --filters "Name=vpc-id,Values=vpc-0931318ae766bff8b" "Name=association.main,Values=false" "Name=route.destination-cidr-block,Values=0.0.0.0/0" "Name=route.gateway-id,Values=local" \
    --query "RouteTables[*].RouteTableId" \
    --output text
