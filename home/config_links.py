import os
import yaml

home_dir=os.getenv("HOME")
src_dir=home_dir+'/.oh-my-zsh/custom/home'

stream = open("install.conf.yaml", 'r')
links = yaml.safe_load(stream)

for key, value in links.items():
  dest = key.replace('home',home_dir)
  dest_exists = os.path.exists(dest)
  src = src_dir + '/' + value

  print ("src: " + src + " dest: " + dest)
  print (dest_exists)

  if dest_exists is False:
    os.symlink(src, dest)
  elif os.path.islink(dest):
    print ("nothing to do")
  elif os.path.isfile(dest):
    os.remove(dest)
    os.symlink(src,dest)
