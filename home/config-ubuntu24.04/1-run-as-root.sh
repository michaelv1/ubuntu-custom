#!/bin/bash

##before you can run this script you need to create a new ubuntu 24.04 VM
##configure shared Clipboard: bidirectional
##configure Drag'n Drop: bidirectional
##configure shared folders

##before you can install the VBOX_GAs_7.1.6 you need to install these:
##apt update && sudo apt install bzip2 gcc make perl
##install VBOX_GAs_7.1.6
 
##reboot the machine and now you can install the rest

# Set up logging
LOG_FILE="/var/log/setup_script.log"
exec > >(tee -i $LOG_FILE) 2>&1

log() {
    echo "[INFO] $(date +'%Y-%m-%d %H:%M:%S') $1"
}

error_handler() {
    echo "[ERROR] $(date +'%Y-%m-%d %H:%M:%S') Command failed at line $1"
    exit 1
}

trap 'error_handler $LINENO' ERR

log "Starting the setup script"

log "Updating and upgrading the system"
apt update && apt upgrade -y

log "Adding deadsnakes PPA and installing Python 3.8"
add-apt-repository ppa:deadsnakes/ppa -y
apt install python3.8 -y

log "Configuring Python alternatives"
update-alternatives --install /usr/bin/python python /usr/bin/python3.8 2
update-alternatives --set python /usr/bin/python3.8

log "Installing dependencies"
apt install bzip2 tar git curl build-essential dkms linux-headers-$(uname -r) libffi-dev libssl-dev python3-dev python3-setuptools python3-wheel python3.8-distutils python3.8-venv dos2unix -y

log "Installing pip for Python 3.8"
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py

log "Setting Python 3.12 as the default Python version"
update-alternatives --install /usr/bin/python python /usr/bin/python3.12 1
update-alternatives --set python /usr/bin/python3.12
python get-pip.py --break-system-packages

log "Installing Python 3.12 venv"
apt install python3.12-venv -y

log "Installing additional packages"
apt update && apt install zsh fzf docker.io wget vim copyq meld jq golang-go preload iftop vnstat iptraf hping3 dstat nmap cl-base64 npm inetutils-traceroute redis-tools locate crudini ipcalc moreutils mysql-client konsole -y

log "Installing Google Chrome"
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb || apt install -f -y
rm google-chrome-stable_current_amd64.deb

log "Installing kubectl"
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.28.13/bin/linux/amd64/kubectl
chmod 755 kubectl
mv kubectl /usr/local/bin

log "Installing kubens and kubectx utilities"
git clone https://github.com/ahmetb/kubectx.git kubectx
cd kubectx
cp kube* /usr/local/bin
chmod 755 /usr/local/bin/kube*
cd ..
rm -rf kubectx

log "Installing VSCode"
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y
apt update
apt install code -y

log "Installing JetBrains IntelliJ IDEA"
snap install intellij-idea-community --classic

log "Installing Groovy"
cd /opt
wget https://mirrors.omnios.org/groovy/apache-groovy-binary-3.0.10.zip
unzip apache-groovy-binary-3.0.10.zip
rm apache-groovy-binary-3.0.10.zip

log "Installing AWS CLI version 2"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install --update
rm -rf aws awscliv2.zip

log "Installing OpenJDK"
apt install openjdk-17-jre-headless -y
apt install openjdk-21-jre-headless -y
update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-17-openjdk-amd64/bin/java 1
update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-21-openjdk-amd64/bin/java 2
update-alternatives --set java /usr/lib/jvm/java-21-openjdk-amd64/bin/java

log "Installing Helm"
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
apt-get update
apt-get install helm -y

log "Adding user michaelv to necessary groups"
usermod -aG vboxsf michaelv
usermod -aG docker michaelv
usermod -aG sudo michaelv

log "Backing up configs"
cp -r /root/home/michaelv /home/michaelv/backup-configs
chown -R michaelv:michaelv /home/michaelv/backup-configs

log "Setup script completed successfully"
