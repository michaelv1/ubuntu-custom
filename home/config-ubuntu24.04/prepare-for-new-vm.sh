#!/usr/bin/env bash

# Check if "generated" folder exists; if yes, remove it
if [ -d "generated" ]; then
  rm -rf "generated"
fi

# Recreate the "generated" folder
mkdir -p "generated"

# Array of top-level directories
DIRS=("git-dev" "git-rnd" "git-devops")

for P in "${DIRS[@]}"; do
  # Full path: ~/git-dev, ~/git-rnd, or ~/git-devops
  FULL_PATH="$HOME/$P"

  # Create a script named after the directory inside "generated"
  SCRIPT_NAME="generated/${P}.sh"

  # Overwrite (or create new) script with a standard shebang
  echo "#!/usr/bin/env bash" > "$SCRIPT_NAME"
  echo "# Auto-generated clone script for $FULL_PATH" >> "$SCRIPT_NAME"
  echo "" >> "$SCRIPT_NAME"

  # Find only the immediate subdirectories
  for d in $(find "$FULL_PATH" -mindepth 1 -maxdepth 1 -type d); do
    dir_name="$(basename "$d")"
    # Append the git clone command to the script
    echo "git clone git@bitbucket.org:anyclip/${dir_name}.git" >> "$SCRIPT_NAME"
  done

  # Make the generated script executable
  chmod +x "$SCRIPT_NAME"

  echo "Created script: ${SCRIPT_NAME}"
done

# Generate the VS Code extensions install script inside "generated"
code --list-extensions | xargs -L 1 echo code --install-extension > "generated/code.extensions.sh"
chmod +x "generated/code.extensions.sh"

echo "Created script: generated/code.extensions.sh"
