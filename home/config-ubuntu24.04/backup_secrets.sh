#!/bin/bash

# Directories to back up
SOURCE_DIRS=(
  "/home/michaelv/.aws"
  "/home/michaelv/.spotinst"
  "/home/michaelv/.bitbucket"
  # Notice we remove /home/michaelv/.ssh from the array for a moment
  "/home/michaelv/.oh-my-zsh/custom/home/config-ubuntu24.04"
)

TMP_BACKUP_DIR="/tmp/backup_$(date +%Y%m%d_%H%M%S)"
mkdir -p "$TMP_BACKUP_DIR"

BACKUP_FILENAME="backup_$(date +%Y%m%d_%H%M%S).tar.gz"
FINAL_BACKUP_DIR="/media/ubuntu_24_04/backups"
mkdir -p "$FINAL_BACKUP_DIR"

echo "Creating backup archive..."

# Archive everything except .ssh normally,
# then add '.ssh' with --dereference
tar -czf "$TMP_BACKUP_DIR/$BACKUP_FILENAME" \
    "${SOURCE_DIRS[@]}" \
    --dereference \
    "/home/michaelv/.ssh"

echo "Moving backup to $FINAL_BACKUP_DIR..."
mv "$TMP_BACKUP_DIR/$BACKUP_FILENAME" "$FINAL_BACKUP_DIR/"

rm -rf "$TMP_BACKUP_DIR"

# Retain only last 2 backups
cd "$FINAL_BACKUP_DIR" || exit
ls -1t | grep '^backup_.*\.tar\.gz$' | tail -n +3 | xargs -r rm --

echo "Backup completed."
