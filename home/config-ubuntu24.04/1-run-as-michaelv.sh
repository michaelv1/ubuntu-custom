#!/bin/bash

# Exit the script if any command fails
set -e

# Variables
BACKUP_DIR="/home/michaelv/backup-configs"
HOME_DIR="/home/michaelv"

# Check and create the backup directory if it does not exist
if [ ! -d "$BACKUP_DIR" ]; then
  echo "Warning: Backup directory $BACKUP_DIR does not exist. Creating it..."
  mkdir -p "$BACKUP_DIR"
  echo "Backup directory created. Please populate it with necessary files."
fi

# Remove existing configurations
echo "Removing existing SSH, Spotinst, and AWS configurations..."
rm -rf "$HOME_DIR/.ssh"
rm -rf "$HOME_DIR/.spotinst"
rm -rf "$HOME_DIR/.aws"
rm -rf "$HOME_DIR/.bitbucket"

# Restore configurations from the backup if files exist
echo "Restoring configurations from backup..."
[ -d "$BACKUP_DIR/.ssh" ] && cp -r "$BACKUP_DIR/.ssh" "$HOME_DIR/.ssh" || echo "No .ssh backup found."
[ -d "$BACKUP_DIR/.spotinst" ] && cp -r "$BACKUP_DIR/.spotinst" "$HOME_DIR/.spotinst" || echo "No .spotinst backup found."
[ -d "$BACKUP_DIR/.aws" ] && cp -r "$BACKUP_DIR/.aws" "$HOME_DIR/.aws" || echo "No .aws backup found."
[ -d "$BACKUP_DIR/.bitbucket" ] && cp -r "$BACKUP_DIR/.bitbucket" "$HOME_DIR/.bitbucket" || echo "No .bitbucket backup found."

chmod 400 /home/michaelv/.ssh/*
ssh-add

# Install tfenv for managing Terraform versions
echo "Installing tfenv..."
if [ ! -d "$HOME_DIR/.tfenv" ]; then
  git clone --depth=1 https://github.com/tfutils/tfenv.git "$HOME_DIR/.tfenv"
else
  echo "tfenv is already installed. Skipping."
fi
sudo ln -sf "$HOME_DIR/.tfenv/bin/"* /usr/local/bin

# Install Oh My Zsh
echo "Installing Oh My Zsh..."
if [ ! -d "$HOME_DIR/.oh-my-zsh" ]; then
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" || {
    echo "Error: Oh My Zsh installation failed.";
    exit 1;
  }
else
  echo "Oh My Zsh is already installed. Skipping."
fi

# Install Homebrew
echo "Installing Homebrew..."
if ! command -v brew &> /dev/null; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  (echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> "$HOME_DIR/.zshrc"
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
else
  echo "Homebrew is already installed. Skipping."
fi

# Install Stern using Homebrew
echo "Installing Stern..."
if ! brew list stern &> /dev/null; then
  brew install stern
  sudo cp /home/linuxbrew/.linuxbrew/Cellar/stern/1.32.0/bin/stern /usr/local/bin/stern
else
  echo "Stern is already installed. Skipping."
fi

# Install related ansible 2.7.18
sudo update-alternatives --set python /usr/bin/python3.8
python -m venv /home/michaelv/python38
source /home/michaelv/python38/bin/activate

pip install --upgrade pip
pip install ansible==2.7.18
pip install ansible-core
pip install ansible-lint
pip install boto3
pip install botocore
pip install Jinja2==3.0.3
pip install MarkupSafe==2.0.1
pip install setuptools==57.5.0
pip install urllib3==1.26.20
pip install tqdm

cd .oh-my-zsh
rm -rf custom
git clone git@bitbucket.org:michaelv1/ubuntu-custom.git custom

cd plugins
git clone https://github.com/zsh-users/zaw.git

cd /home/michaelv/.oh-my-zsh/custom/home
config_vscode.sh
python config_links.py

echo "Setup completed successfully."

shutdown -r now