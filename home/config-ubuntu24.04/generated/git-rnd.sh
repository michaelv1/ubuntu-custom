#!/usr/bin/env bash
# Auto-generated clone script for /home/michaelv/git-rnd

git clone git@bitbucket.org:anyclip/discrepancy-to-redshift.git
git clone git@bitbucket.org:anyclip/mp-k6-loadtest.git
git clone git@bitbucket.org:anyclip/anyclip-pcn-manager-api.git
git clone git@bitbucket.org:anyclip/tagger-helper.git
git clone git@bitbucket.org:anyclip/anyclip-ad-manager-proxy.git
git clone git@bitbucket.org:anyclip/anyclip-user-sync-api.git
git clone git@bitbucket.org:anyclip/thumbnails.git
git clone git@bitbucket.org:anyclip/anyclip-email-service.git
git clone git@bitbucket.org:anyclip/hls-backfill.git
git clone git@bitbucket.org:anyclip/tagger-help.git
git clone git@bitbucket.org:anyclip/dmp_errors.git
git clone git@bitbucket.org:anyclip/social-sharing.git
git clone git@bitbucket.org:anyclip/direct-whiteops-by-tag-316-redshift.git
git clone git@bitbucket.org:anyclip/form-sender.git
git clone git@bitbucket.org:anyclip/anyclip-pcn-nginx.git
git clone git@bitbucket.org:anyclip/anyclip-memcached-backup.git
git clone git@bitbucket.org:anyclip/lre-events.git
git clone git@bitbucket.org:anyclip/anyclip-pcn-augmentor.git
git clone git@bitbucket.org:anyclip/gateway-usage.git
git clone git@bitbucket.org:anyclip/nifi-gw.git
git clone git@bitbucket.org:anyclip/anyclip-video-gallery.git
git clone git@bitbucket.org:anyclip/anyclip-zoom-service.git
git clone git@bitbucket.org:anyclip/devops-config-synthetic-checks.git
git clone git@bitbucket.org:anyclip/anyclip-tt.git
git clone git@bitbucket.org:anyclip/video-upload.git
git clone git@bitbucket.org:anyclip/pcn2rs.git
git clone git@bitbucket.org:anyclip/anyclip-teams-service.git
git clone git@bitbucket.org:anyclip/mrss-generator.git
git clone git@bitbucket.org:anyclip/anyclip-sharepoint-service.git
