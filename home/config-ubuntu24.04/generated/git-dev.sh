#!/usr/bin/env bash
# Auto-generated clone script for /home/michaelv/git-dev

git clone git@bitbucket.org:anyclip/devops-terraform-modules.git
git clone git@bitbucket.org:anyclip/infrastructure.git
git clone git@bitbucket.org:anyclip/devops-ftp-paligo.git
git clone git@bitbucket.org:anyclip/devops-account-infra.git
git clone git@bitbucket.org:anyclip/devops-configuration-infra.git
git clone git@bitbucket.org:anyclip/devops-nexus.git
git clone git@bitbucket.org:anyclip/devops-config-nexus-repository.git
git clone git@bitbucket.org:anyclip/devops-config-mrss-cdns.git
git clone git@bitbucket.org:anyclip/devops-configuration.git
git clone git@bitbucket.org:anyclip/devops-config-commons.git
git clone git@bitbucket.org:anyclip/devops-terraform14-modules.git
git clone git@bitbucket.org:anyclip/devops-demo.git
git clone git@bitbucket.org:anyclip/devops-config-env-builder.git
git clone git@bitbucket.org:anyclip/devops-config-env-buckets.git
git clone git@bitbucket.org:anyclip/anyclip-infrastructure.git
git clone git@bitbucket.org:anyclip/devops-config-fluentd-logging.git
git clone git@bitbucket.org:anyclip/devops-jenkins-lib.git
git clone git@bitbucket.org:anyclip/devops-kubernetes-clusters.git
git clone git@bitbucket.org:anyclip/devops-config-permissions.git
git clone git@bitbucket.org:anyclip/devops-config-s3-policy-limelight-updater.git
git clone git@bitbucket.org:anyclip/devops-general-repo.git
git clone git@bitbucket.org:anyclip/devops-containers.git
git clone git@bitbucket.org:anyclip/devops-config-shared.git
git clone git@bitbucket.org:anyclip/devops-common-helm.git
git clone git@bitbucket.org:anyclip/devops-config-network.git
git clone git@bitbucket.org:anyclip/devops-config-chartmuseum.git
git clone git@bitbucket.org:anyclip/infrastructure.local.git
git clone git@bitbucket.org:anyclip/devops-config-jenkins.git
git clone git@bitbucket.org:anyclip/devops-config-iam-policy.git
