code --install-extension ahebrank.yaml2json
code --install-extension alefragnani.bookmarks
code --install-extension alefragnani.project-manager
code --install-extension amodio.toggle-excluded-files
code --install-extension arturock.gitstash
code --install-extension atlassian.atlascode
code --install-extension christian-kohler.path-intellisense
code --install-extension docsmsft.docs-yaml
code --install-extension donjayamanne.githistory
code --install-extension dotjoshjohnson.xml
code --install-extension eriklynd.json-tools
code --install-extension felipecaputo.git-project-manager
code --install-extension genieai.chatgpt-vscode
code --install-extension hashicorp.terraform
code --install-extension ilevine.kubesquash
code --install-extension ipedrazas.kubernetes-snippets
code --install-extension kappariver.git-open-diff
code --install-extension keewek.compare-helper
code --install-extension letmaik.git-tree-compare
code --install-extension maattdd.gitless
code --install-extension mattiasbaake.vscode-snippets-for-ansible
code --install-extension mgesbert.python-path
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools
code --install-extension ms-python.debugpy
code --install-extension ms-python.isort
code --install-extension ms-python.python
code --install-extension ms-python.vscode-pylance
code --install-extension ms-toolsai.jupyter
code --install-extension ms-toolsai.jupyter-keymap
code --install-extension ms-toolsai.jupyter-renderers
code --install-extension ms-toolsai.vscode-jupyter-cell-tags
code --install-extension ms-toolsai.vscode-jupyter-slideshow
code --install-extension ms-vscode-remote.remote-containers
code --install-extension naturalethic.es6-string-yaml
code --install-extension nickdemayo.vscode-json-editor
code --install-extension ramiroberrelleza.bitbucket-pull-requests
code --install-extension redhat.vscode-yaml
code --install-extension samuelcolvin.jinjahtml
code --install-extension sanaajani.taskrunnercode
code --install-extension tberman.json-schema-validator
code --install-extension tobermory.es6-string-html
code --install-extension vscode-icons-team.vscode-icons
code --install-extension waderyan.gitblame
code --install-extension wholroyd.jinja
