terraform_inspect_sensitive(){
    # Temporary files for storing plan and JSON output
    local PLANFILE=$(mktemp /tmp/plan.XXX.tfplan)
    local JSONFILE=$(mktemp /tmp/plan.XXX.json)

    # Cleanup function to remove temporary files on exit or interrupt
    cleanup() {
    rm -f "${PLANFILE}" "${JSONFILE}"
    }
    trap cleanup EXIT

    # Generate Terraform plan and convert it to JSON
    echo "Generating Terraform plan..."
    terraform plan -out="${PLANFILE}" || exit 1
    terraform show -json "${PLANFILE}" > "${JSONFILE}" || exit 1

    # Read resource changes into an array
    echo "Extracting resource changes..."
    # Zsh-compatible alternative for readarray
    changes=()
    while IFS= read -r line; do
    changes+=("$line")
    done < <(jq --compact-output '.resource_changes[]' "${JSONFILE}")

    # Iterate through changes and compare before/after states
    for change in "${changes[@]}"; do
        # Extract before and after states
        local before=$(jq '.change.before' <<< "$change" | fold -s -w 80)
        local after=$(jq '.change.after' <<< "$change" | fold -s -w 80)

        # Check if there are differences
        if [[ "$before" != "$after" ]]; then
            # Print resource address and type for context
            address=$(jq --raw-output '.address' <<< "$change")
            type=$(jq --raw-output '.type' <<< "$change")

            echo "Changes detected in resource: ${address} (type: ${type})"
            echo "---------------------------------------------"

            # Show differences with wrapped lines
            meld <(echo "${before}") <(echo "${after}")
        fi
    done
}