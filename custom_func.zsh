alias ll="ls -alh"
alias pause-workers="curl http://ac-prod-video-upload-int.anyclipsrv.info/pause_workers"
alias resume-workers="curl http://ac-prod-video-upload-int.anyclipsrv.info/resume_workers"
alias pause-workers-eu="curl http://ac-euprod-video-upload-int.eu.anyclip.com/pause_workers"
alias resume-workers-eu="curl http://ac-euprod-video-upload-int.eu.anyclip.com/resume_workers"
alias gcm="git add . && git commit --amend --no-edit && git push --force"
alias gs="git status"
alias kgpyaml="kubectl get pod $1 -o yaml"
alias alblogs="stern aws-load-balancer-controller -ndevops"
alias dnslogs="stern external-dns -ndevops"
alias karpenterlogs="stern karpenter -nkarpenter"
alias ssh-firewall='ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no admin@192.168.10.254'
alias docker-login='aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 056612754658.dkr.ecr.us-east-1.amazonaws.com'
alias buildah-login='aws ecr get-login-password --region us-east-1 | buildah login --username AWS --password-stdin 056612754658.dkr.ecr.us-east-1.amazonaws.com'
alias k8-check-aggregator='kubectl get pods -o wide | grep lre-events-aggregator-cronjob | grep Running'
alias ap='ansible-playbook `find *local.yml`'
alias k8-list-endpoint="kubectl get ingresses.extensions -oyaml | grep -e \"- host:\""
alias k8-kv="eks-node-viewer --resources cpu,memory -extra-labels instancegroup"
alias k8-kl='kubectl logs -f -n karpenter -c controller -l app.kubernetes.io/name=karpenter'
alias grep="grep -Ei --color=auto"
alias nifilogs="kubetail ac-dev-nifi- | grep -v \"INFO|DEBUG|zookeeper\""
alias k8-helm-today="helm ls | grep $(date "+%Y-%m-%d") | grep -v network"
alias kg="kubectl get"

export GIT_EDITOR=vim
k8-create-job () {
  select=`kubectl get cronjob | awk '{print $1}' | grep -v 'NAME' | sort -n | fzf --inline-info -0`
  kubectl create job --from=cronjob/$select temporary-$select
}

dmarc-move() {
  cd ~/.oh-my-zsh/custom/python_files && python read_dmarc.py && cd - 
}

tr-debug-enable() {
  export TF_LOG=DEBUG
}

tr-debug-disable() {
  unset TF_LOG
}

k8-upload-log-s3() {
  local select
  select=`kubectl get pods | grep $1 | awk '{print $1}' | sort -n | fzf --exit-0`
  echo $select
  kubectl logs $select > /tmp/$select.log
  aws s3 cp /tmp/$select.log s3://anyclip-devops/michael/$select.log
  }

k8-check-pod-role() {
  local select
  select=`kubectl get pods | awk '{print $1}' | sort -n | fzf --exit-0`
  echo $select
  kubectl exec $select -- curl -s "http:/169.254.169.254/latest/meta-data/iam/security-credentials/"  
  }

k8-port-forward-pod() {
  local select
  select=`kubectl get pods --no-headers | awk '{print $1}' | sort -n | fzf --exit-0`
  echo $select
  kubectl port-forward pod/$select $1
  }

k8-select-cluster() {
  select=`find ~/.oh-my-zsh/custom/kubeconfig/* -type d | sort -n | fzf --exit-0`
  cluster=$(basename $select)
  echo $cluster
  export KOPS_CLUSTER_NAME=$cluster
  source ~/.oh-my-zsh/custom/kubeconfig/$cluster/state.sh
}

k8-port-forward-service() {
  local select
  select=`kubectl get svc --no-headers | awk '{print $1}' | sort -n | fzf --exit-0`
  echo $select
  kubectl port-forward svc/$select $1
  }

k8-check-failures() {
  kubectl get pods --all-namespaces -o wide | grep -viE "Running|Completed"
  kubectl get nodes | grep -iE "master|SchedulingDisabled"
  }

k8-get-pasword() {
  local select
  select=`aws secretsmanager list-secrets --output json | jq -r '.SecretList[] | "\(.Name)"' | sort -n | fzf --exit-0`
  aws secretsmanager get-secret-value --secret-id $select --output json | jq -r '.SecretString' | jq -r 'to_entries[] | "\(.value)"'
  }	

k8-check-repo-updates() {
  cd ~/git-devops
  select=$(ls -d * | grep "devops-config" | sort -n | fzf --exit-0)
  clear
  cd "$select"
  reference_branch="ac-dev"
  
  # Only include ac-dev, ac-qa, ac-prod, ac-euprod and exclude HEAD -> lines
  git branch -r \
    | grep -v 'HEAD ->' \
    | grep -E 'origin/(ac-dev|ac-qa|ac-prod|ac-euprod)$' \
    > branches.txt

  run_git_diff() {
    local other_branch="$1"
    local diff_output
    diff_output=$(git diff "$reference_branch..$other_branch")

    if [ -n "$diff_output" ]; then
      echo "Comparing $other_branch with $reference_branch:"
      echo "$diff_output"
    else
      echo "No differences found between $other_branch and $reference_branch."
    fi
  }

  while IFS= read -r line; do
    # Clean up the line to remove 'origin/' and any extra spaces
    line=$(echo "$line" | sed 's/origin\///;s/ //g')
    echo "Processing branch: $line"
    git checkout "$line" && git pull
    run_git_diff "$line"
  done < branches.txt

  git checkout "$reference_branch"
  rm branches.txt
  cd ../
}
 
k8-describe-node() {
  instance_id=$1
  k describe node `aws ec2 describe-instances --filters Name=instance-id,Values=${instance_id} --query 'Reservations[*].Instances[*].PrivateDnsName' --output text`
  }

ketin() {
  keti $1 -n`kgp --all-namespaces | grep $1 | awk '{print $1}'` -- $2
  }

klfn() {
  klf $1 -c $2 -n`kgp --all-namespaces | grep $1 | awk '{print $1}'`
  }

kdpn() {
  kdp $1 -n`kgp --all-namespaces | grep $1 | awk '{print $1}'`
  }

time-it() {
  local start=$SECONDS ts ec
  printf -v ts '%(%Y-%m-%d_%H:%M:%S)T' -1
  printf '%s\n' "$ts Starting $*"
  "$@"; ec=$?
  printf -v ts '%(%Y-%m-%d_%H:%M:%S)T' -1
  printf '%s\n' "$ts Finished $*; elapsed = $((SECONDS-start)) seconds"
  # make sure to return the exit code of the command so that the caller can use it
  return "$ec"
}

tf166() {

  tfenv use 1.6.6 && rm .terraform -rf || true && terraform init -lock=false && terraform $1

}

tf192() {

  tfenv use 1.9.2 && rm .terraform* -rf || true && terraform init -lock=false && terraform $1

}

k8-create-secret-to-move() {

  vared -p 'select the namespace to move to?:' -c ns
  select=`kubectl get secrets | grep Opaque | awk '{print $1}' | sort -n | fzf --exit-0`
  kubectl get secret ${select} -o yaml | sed "s/namespace: .*/namespace: $ns/" > ~/secrets/${ns}_${select}.yaml
  echo "secrets/${ns}_${select}.yaml"
  cat ~/secrets/${ns}_${select}.yaml

}

k8-env-versions() {
  kubectl get deployments.apps -o=jsonpath="{range.items[*]}{.metadata.namespace}{','}{.metadata.name}{','}{.spec.replicas}{','}{range .spec.template.spec.containers[*]}{.image}{','}{'\n'}{end} {'\n'}{end}" | sort -k1 -k2 | awk 'BEGIN {FS=",|:"}; {print $1,$2,$5}'
  kubectl get statefulsets.apps -o=jsonpath="{range.items[*]}{.metadata.namespace}{','}{.metadata.name}{','}{.spec.replicas}{','}{range .spec.template.spec.containers[*]}{.image}{','}{'\n'}{end} {'\n'}{end}" | sort -k1 -k2 | awk 'BEGIN {FS=",|:"}; {print $1,$2,$5}'

}

k8-git-fetch-all() {
  for branch in  `git branch`;
  do 
    git checkout $branch && git pull
  done
  git checkout ac-prod
  git checkout ac-dev
  git diff ac-prod
}

k8-create-rds-snapshot() {
  select=`aws rds describe-db-instances | jq -r '.DBInstances[] | "\(.DBInstanceIdentifier)"' | fzf`
  echo $select
  aws rds create-db-snapshot --db-snapshot-identifier $select-`date +"%Y%m%d%H%M%S"` --db-instance-identifier $select
}

k8-check-rds-snapshot-status() {

  aws rds describe-db-snapshots | jq -r '.DBSnapshots[] | select (.Status != "available") | "\(.Status), \(.DBInstanceIdentifier),\(.DBSnapshotIdentifier)"' |
  while read snapshot; do
    echo "Snapshot with non-available status: $snapshot"
  done
}

k8-get-kubectl-version() {

cd /tmp
version=`kubectl version --output=json | jq -r '.serverVersion.gitVersion' | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+'`
curl -LO https://dl.k8s.io/release/v$version/bin/linux/amd64/kubectl
chmod +x kubectl
./kubectl version

}

function helmdryrun() {
    local python_code=$(cat <<'EOF'
import yaml
import subprocess
import glob

def find_local_yaml_file():
    # Search for *-local.yml files in the current directory
    yaml_files = glob.glob("*-local.yml")
    
    if not yaml_files:
        print("Error: No *-local.yml files found in the current directory.")
        return None
    
    if len(yaml_files) > 1:
        print("Warning: Multiple *-local.yml files found. Using the first one.")

    return yaml_files[0]

def helm_upgrade_from_yaml(yaml_file):
    # Read the YAML file
    with open(yaml_file, 'r') as file:
        yaml_data = yaml.safe_load(file)

    # Extract ROLE and ENV
    role = yaml_data[0]['vars']['ROLE']
    env = yaml_data[0]['vars']['ENV']

    # Execute helm upgrade command
    helm_command = f"helm upgrade -i --debug --dry-run {env}-{role} {role}/helm/{role}"
    subprocess.run(helm_command, shell=True, check=True)

# Find the first *-local.yml file in the current directory
yaml_file = find_local_yaml_file()

if yaml_file:
    helm_upgrade_from_yaml(yaml_file)
else:
    print("Exiting.")
EOF
)

    # Run the Python code
    python -c "$python_code"
}

k8-buildah-build() {
  local ROLE=$1
  aws ecr get-login-password --region us-east-1 | buildah login --username AWS --password-stdin 056612754658.dkr.ecr.us-east-1.amazonaws.com
  buildah build -f ./Dockerfile -t $ROLE
  
}

k8-drain-node() {
  select=`kubectl get nodes | sort -n | fzf --exit-0`
  node=`echo $select | awk '{print $1}'`
  echo $node
  kubectl drain $node --ignore-daemonsets --delete-emptydir-data
}

k8-backup-nifi() {
    # Get the current Kubernetes namespace
    local NS=$(kubectl config view --minify --output 'jsonpath={..namespace}')
    if [[ -z "$NS" ]]; then
        echo "No namespace set in the current context, defaulting to 'default'"
        NS="default"
    fi

    # Define backup directory
    local BACKUP_DIR="$HOME/nifi-backups/$NS"
    [[ -d "$BACKUP_DIR" ]] || mkdir -p "$BACKUP_DIR"

    # Get current timestamp
    local TIMESTAMP=$(date +"%Y%m%d-%H%M%S")

    # Get NiFi pods and back up their flows
    for pod in $(kubectl get pods -n "$NS" --no-headers --field-selector=status.phase=Running | awk '$1 ~ /nifi-[0-9]+$/ {print $1}'); do
        echo "Backing up from $pod in namespace $NS..."
        
        kubectl cp -n "$NS" "$pod:/opt/nifi/data/flow.xml.gz" "$BACKUP_DIR/$pod-flow-$TIMESTAMP.xml.gz" 2>&1 | tee -a "$BACKUP_DIR/backup.log"
        kubectl cp -n "$NS" "$pod:/opt/nifi/data/flow.json.gz" "$BACKUP_DIR/$pod-flow-$TIMESTAMP.json.gz" 2>&1 | tee -a "$BACKUP_DIR/backup.log"
    done

    echo "Backup completed at $BACKUP_DIR"
}

k8-restore-nifi-flow() {
  local backup_dir="$HOME/nifi-backups"

  # Check if kubectl is installed
  if ! command -v kubectl &>/dev/null; then
    echo "Error: kubectl is not installed or not in PATH."
    return 1
  fi

  # Get the current namespace from KUBECONFIG (this is the restore destination)
  local ns_to
  ns_to=$(kubectl config view --minify --output 'jsonpath={..namespace}')

  if [[ -z "$ns_to" ]]; then
    echo "Error: Could not determine the current namespace from KUBECONFIG."
    return 1
  fi

  echo "Current namespace detected as restore target: $ns_to"

  # Get list of available namespaces from backup directory
  local namespaces=($(ls -d "$backup_dir"/*/ 2>/dev/null | xargs -n 1 basename))

  if [[ ${#namespaces[@]} -eq 0 ]]; then
    echo "No backup namespaces found in $backup_dir"
    return 1
  fi

  echo "Select a namespace to restore from (source backup namespace):"
  select ns_from in "${namespaces[@]}"; do
    if [[ -n "$ns_from" ]]; then
      break
    else
      echo "Invalid selection, please try again."
    fi
  done

  # Find the latest backup file in the selected source namespace
  local restore_file
  restore_file=$(ls -t "$backup_dir/$ns_from/"*.json.gz 2>/dev/null | head -n 1)

  if [[ -z "$restore_file" ]]; then
    echo "No backup files found in $backup_dir/$ns_from/"
    return 1
  fi

  # Define the target StatefulSet and pod in the current namespace
  local statefulset="${ns_to}-nifi"
  local pod="${statefulset}-0"  # First pod in the StatefulSet

  # Check if the target pod exists
  if ! kubectl get pod -n "$ns_to" "$pod" &>/dev/null; then
    echo "Error: No running NiFi pod found with the name $pod in namespace $ns_to"
    return 1
  fi

  # Confirmation before restoring (Zsh-friendly)
  echo "Found latest backup in source namespace $ns_from: $restore_file"
  echo "This will restore the file to $ns_to/$pod:/opt/nifi/data/flow.json.gz"
  echo -n "Proceed with restore? (y/n): " 
  read confirm

  if [[ "$confirm" != "y" ]]; then
    echo "Restore aborted."
    return 0
  fi

  # Perform restore
  echo "Restoring $restore_file to $ns_to/$pod:/opt/nifi/data/flow.json.gz"
  kubectl cp "$restore_file" "$ns_to/$pod:/opt/nifi/data/flow.json.gz"

  if [[ $? -eq 0 ]]; then
    echo "Restore completed successfully."
  else
    echo "Error: Restore failed."
  fi
}

#!/usr/bin/env bash

function k8-pull-repo() {
  # 1. Retrieve the list of all Helm releases in JSON format (-A means all namespaces)
  local helm_list_json
  helm_list_json="$(helm ls --all -o json)"

  # If helm_list_json is empty, either no releases exist or there's an error
  if [[ -z "$helm_list_json" ]]; then
    echo "helm ls returned empty JSON or encountered an error."
    return 1
  fi

  # 2. Convert JSON to lines of the form: NAME|NAMESPACE|CHART
  local selection
  selection="$(
    echo "$helm_list_json" \
      | jq -r '.[] | "\(.name)|\(.namespace)|\(.chart)"' \
      | sort \
      | fzf --exit-0
  )"

  # If no selection was made or there's nothing to pick, exit
  if [[ -z "$selection" ]]; then
    echo "No selection made or no Helm releases found."
    return 1
  fi

  # 3. Split the selection on '|'
  local release_name
  local release_namespace
  local chart
  IFS='|' read -r release_name release_namespace chart <<< "$selection"

  # Show what we got
  echo "NAME        = $release_name"
  echo "NAMESPACE   = $release_namespace"
  echo "CHART       = $chart"

  # 4. Derive the 'service' by removing the '<NAMESPACE>-' prefix from NAME
  #    e.g., if NAME is 'ac-dev-weavo-ml-features' and NAMESPACE is 'ac-dev',
  #    then service becomes 'weavo-ml-features'.
  local service="${release_name#${release_namespace}-}"

  # 5. Derive the 'version' by removing the '<service>-' prefix from CHART
  #    e.g., if CHART is 'weavo-ml-features-0.2.0-3-v60-ac-dev' and
  #    service is 'weavo-ml-features', then version becomes '0.2.0-3-v60-ac-dev'.
  local version="${chart#${service}-}"

  echo "SERVICE     = $service"
  echo "VERSION     = $version"

  # 6. Pull the chart from 'anyclip-charts/<service>' with '--version <version>'
  echo "Pulling chart from anyclip-charts repo..."
  helm pull "anyclip-charts/$service" --version "$version" --destination .
}

function kgpw() {
  local service="$1"
  
  if [[ -z "$service" ]]; then
    echo "Usage: k8-find-service-namespace <service-name>"
    return 1
  fi

  # Find the namespace where the service is installed
  local namespace
  namespace=$(kubectl get pods --all-namespaces | grep "$service" | awk '{print $1}' | head -n 1)

  if [[ -z "$namespace" ]]; then
    echo "Service '$service' not found in any namespace."
    return 1
  fi

  echo "Service '$service' found in namespace '$namespace'."

  # Run kgpw -n<namespace> | grep <service>
    kubectl get pods -n"$namespace" -owide --watch | grep "$service"
}

# Usage:
# k8-find-service-namespace <service-name>

# k8-create-branch
#
# 1) Modifies the "vars:" block in ~/git-dev/devops-templater/local.yml, replacing
#      ROLE:                ->  "SERVICE"
#      DEVOPS_CONFIG_BRANCH ->  "BRANCH_NAME"
#      CONFIGURATION_BRANCH ->  "BRANCH_NAME"
#    but ONLY between 'vars:' and 'roles:' lines (i.e., the same block).
#
# 2) Creates/pushes a new branch in:
#    - ~/git-dev/devops-configuration (checkout master, pull, create branch)
#    - ~/git-devops/devops-config-$SERVICE (checkout ac-dev, pull, create branch)
#
# Usage:
#   k8-create-branch
#     (prompts for SERVICE and BRANCH_NAME)
#   k8-create-branch <SERVICE> <BRANCH_NAME>
#     (uses the provided arguments directly)

k8-create-branch() {

  ##########################################################################
  # 0) Prompt for SERVICE / BRANCH_NAME if not provided as arguments
  ##########################################################################
  if [[ -z "$1" ]]; then
    read -r "SERVICE?Enter service name (e.g., billing): "
  else
    SERVICE="$1"
  fi

  if [[ -z "$2" ]]; then
    read -r "BRANCH_NAME?Enter branch name (e.g., feature/BILL-101): "
  else
    BRANCH_NAME="$2"
  fi

  echo "Creating new branch '$BRANCH_NAME' for service '$SERVICE'."
  echo "  Repo 1: ~/git-dev/devops-configuration"
  echo "  Repo 2: ~/git-devops/devops-config-$SERVICE"
  echo "Will attempt to update: ~/git-dev/devops-templater/local.yml"
  read -r "CONTINUE?Do you want to continue? (y/n) "

  if [[ "$CONTINUE" != "y" ]]; then
    echo "Operation canceled."
    return 1
  fi

  ##########################################################################
  # 1) Helper function: sed_in_place (handles macOS vs Linux for 'sed -i')
  ##########################################################################
  sed_in_place() {
    if [[ "$OSTYPE" == "darwin"* ]]; then
      # macOS (BSD sed) - must provide a backup extension (even if empty)
      sed -i '' "$@" 
    else
      # Linux (GNU sed)
      sed -i "$@"
    fi
  }

  ##########################################################################
  # 2) Update local.yml in the "vars:" block (stop at "roles:")
  ##########################################################################
  LOCAL_YML="$HOME/git-dev/devops-templater/local.yml"

  if [[ ! -f "$LOCAL_YML" ]]; then
    echo "Error: $LOCAL_YML not found. Exiting."
    return 1
  fi

  echo "Modifying $LOCAL_YML under 'vars:'..."

  # We'll run one 'sed' command with a range:
  #  - Start at any line matching "vars:" (allowing indentation)
  #  - End at any line matching "roles:" (same indentation level)
  # Inside that range, we do three s/// commands for ROLE, DEVOPS_CONFIG_BRANCH, CONFIGURATION_BRANCH
  #
  # The replacement lines use two spaces of indentation. If your file uses a different indentation,
  # adjust the "  ROLE:" to match. Right now, your file has:
  #
  #   vars:
  #     ROLE: "..."
  #     DEVOPS_CONFIG_BRANCH: "..."
  #     CONFIGURATION_BRANCH: "..."
  #
  # So we keep two spaces before ROLE: in the replacement.

  sed_in_place "/^[[:blank:]]*vars:[[:blank:]]*$/,/^[[:blank:]]*roles:[[:blank:]]*$/ {
    s/^[[:blank:]]*ROLE:[[:blank:]]*.*/  ROLE: \"$SERVICE\"/
    s/^[[:blank:]]*DEVOPS_CONFIG_BRANCH:[[:blank:]]*.*/  DEVOPS_CONFIG_BRANCH: \"$BRANCH_NAME\"/
    s/^[[:blank:]]*CONFIGURATION_BRANCH:[[:blank:]]*.*/  CONFIGURATION_BRANCH: \"$BRANCH_NAME\"/
  }" "$LOCAL_YML"

  ##########################################################################
  # 3) devops-configuration: master -> new branch
  ##########################################################################
  if [[ -d "$HOME/git-dev/devops-configuration" ]]; then
    echo "=== Working on ~/git-dev/devops-configuration ==="
    cd "$HOME/git-dev/devops-configuration" || {
      echo "Error: Could not change directory to ~/git-dev/devops-configuration."
      return 1
    }

    echo "Switching to branch: master"
    git checkout master
    echo "Pulling latest changes..."
    git pull

    echo "Creating and checking out new branch: $BRANCH_NAME"
    git checkout -b "$BRANCH_NAME"
    echo "Pushing branch: $BRANCH_NAME"
    git push -u origin "$BRANCH_NAME"
  else
    echo "Warning: Directory ~/git-dev/devops-configuration does not exist. Skipped."
  fi

  ##########################################################################
  # 4) devops-config-$SERVICE: ac-dev -> new branch
  ##########################################################################
  if [[ -d "$HOME/git-devops/devops-config-$SERVICE" ]]; then
    echo "=== Working on ~/git-devops/devops-config-$SERVICE ==="
    cd "$HOME/git-devops/devops-config-$SERVICE" || {
      echo "Error: Could not change directory to ~/git-devops/devops-config-$SERVICE."
      return 1
    }

    echo "Switching to branch: ac-dev"
    git checkout ac-dev
    echo "Pulling latest changes..."
    git pull

    echo "Creating and checking out new branch: $BRANCH_NAME"
    git checkout -b "$BRANCH_NAME"
    echo "Pushing branch: $BRANCH_NAME"
    git push -u origin "$BRANCH_NAME"
  else
    echo "Warning: Directory ~/git-devops/devops-config-$SERVICE does not exist. Skipped."
  fi

  echo "Done."
}

function delete_empty_branch() {
  # If a branch name wasn't passed as an argument, prompt the user
  if [[ -z "$1" ]]; then
    read -r "BRANCH?Enter the name of the branch you want to delete: "
  else
    BRANCH="$1"
  fi

  # Check if the branch exists locally
  if ! git rev-parse --verify "$BRANCH" >/dev/null 2>&1; then
    echo "Branch '$BRANCH' does not exist locally."
    return 1
  fi

  # Check if there are any commits in $BRANCH that are not in master
  # If this is empty, the branch is fully contained in (or identical to) master.
  if [[ -z "$(git log master.."$BRANCH" --oneline 2>/dev/null)" ]]; then
    echo "Branch '$BRANCH' has no unique commits (empty or fully merged). Deleting..."

    # Delete the local branch
    git branch -d "$BRANCH"

    # Check if branch exists on remote, and if so, delete it there too
    if git ls-remote --exit-code --heads origin "$BRANCH" >/dev/null 2>&1; then
      echo "Deleting '$BRANCH' from remote..."
      git push origin --delete "$BRANCH"
    else
      echo "Branch '$BRANCH' not found on remote. Skipping remote deletion."
    fi

  else
    echo "Branch '$BRANCH' has commits not in 'master'. Will not delete automatically."
  fi
}
