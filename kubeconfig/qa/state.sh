export AWS_PROFILE="dev-account"
export FASTLY_API_KEY="RxKz_VMP6sDPVxAdingXTosM6xw702S-"

# Check if AWS SSO credentials are valid
aws sts get-caller-identity > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "You already have valid AWS SSO credentials."
else
  echo "AWS SSO credentials are not valid. Running 'aws sso login'..."
  aws sso login --profile dev-account
fi
sed -i 's/UTC/Z/g' ~/.aws/sso/cache/*
export KUBECONFIG=~/.oh-my-zsh/custom/kubeconfig/$KOPS_CLUSTER_NAME/config
if [ ! -f $KUBECONFIG ];
  then
    aws eks update-kubeconfig --name $KOPS_CLUSTER_NAME 
fi

chmod 600 ~/.oh-my-zsh/custom/kubeconfig/$KOPS_CLUSTER_NAME/config
export PATH=~/.oh-my-zsh/custom/kubeconfig/$KOPS_CLUSTER_NAME:$PATH
export NODES_SSH_USER="ec2-user"

if [[ "$GIO_LAUNCHED_DESKTOP_FILE" == "/usr/share/applications/code.desktop" ]];
  then
    kubeoff
fi

echo -ne "\033]30;$KOPS_CLUSTER_NAME\007"
