awsGetToken() {
  local aws_mfs_sn='arn:aws:iam::056612754658:mfa/MichaelVaknin'
  local token=$1
  [[ x${token} == x ]] && read "token? Enter Google Authenticatior token: "
  eval $(\
    aws sts get-session-token --serial-number ${aws_mfs_sn} --token-code ${token} --profile access-key |\
    awk -F'"' '\
      /AccessKeyId/{print "crudini --set ~/.aws/credentials default aws_access_key_id", $(NF-1) ";"};
      /SecretAccessKey/{print "crudini --set ~/.aws/credentials default aws_secret_access_key", $(NF-1) ";"};
      /SessionToken/{print "crudini --set ~/.aws/credentials default aws_session_token", $(NF-1) ";"}
  ')
}

alias alogin=awsGetToken
