# Function to establish an SSH connection to an Amazon EC2 instance.
# It shows the user a list of pods and lets them select the server of the pod to SSH into.
#
# Parameters:
#   None
ssh-ec2-pod-instance() {

    local select=$(kubectl get pods -o wide | sort -n | fzf --exit-0)
    if [ -z "$select" ]; then
        echo "No pod selected."
        return 1
    fi

    local server=$(echo "$select" | awk '{for (i=1; i<=NF; i++) if ($i ~ /^[a-zA-Z0-9.-]+\.(ec2.internal)$/) print $i; exit}')
    echo "Selected server: $server"

    if [ -z "$server" ]; then
        echo "Server address not found."
        return 1
    fi

    # Fetching the instance ID and Private IP from AWS.
    local aws_output=$(aws ec2 describe-instances \
                            --filters "Name=private-dns-name,Values=$server" \
                            --query "Reservations[*].Instances[*].[InstanceId, PrivateIpAddress]" \
                            --output text)
    if [ -z "$aws_output" ]; then
        echo "AWS query failed or instance not found."
        return 1
    fi

    local instance_id=$(echo "$aws_output" | awk '{print $1}')
    local private_ip=$(echo "$aws_output" | awk '{print $2}')

    if [ -z "$instance_id" ] || [ -z "$private_ip" ]; then
        echo "Failed to retrieve essential instance information."
        return 1
    fi

    echo "Instance ID: $instance_id"
    echo "Private IP: $private_ip"

    # SSH using EC2 Instance Connect
    aws ec2-instance-connect ssh --os-user ec2-user --instance-id $instance_id --connection-type eice
}

# Function to establish an SSH connection to an Amazon EC2 instance.
# It allows direct connection if IP and username are known, or lets the user select an instance interactively.
#
# Parameters:
#   $1: User (string) - Username for the SSH connection.
#   $2: IP (string) - IP address of the EC2 instance to directly connect.
ssh-ec2-instance() {
    local user=$1
    local ip=$2

    # Check parameters, use them if defined 
    if [[ -n $user && -n $ip ]]; then
        echo "Direct SSH to IP: $ip with user: $user"
        local instance_id=$(aws ec2 describe-instances \
                            --filters "Name=private-ip-address,Values=$ip" \
                            --query "Reservations[*].Instances[*].InstanceId" \
                            --output text)
        if [[ -z $instance_id ]]; then
            echo "No instance found with IP $ip."
            return 1
        fi

        # SSH using EC2 Instance Connect
        aws ec2-instance-connect ssh --os-user $user --instance-id $instance_id --connection-type eice
    else
        local instances
        echo "Fetching EC2 instances..."
        instances=$(aws ec2 describe-instances \
                    --filters "Name=instance-state-name,Values=running" \
                    --query "Reservations[*].Instances[*].[InstanceId, Tags[?Key=='Name'].Value | [0], State.Name, PublicIpAddress, PrivateIpAddress]" \
                    --output text | awk '{print $1 " (" $2 ") [" $3 "] - Public IP: " $4 ", Private IP: " $5}')

        [ -z "$instances" ] && { echo "No instances found."; return 1; }

        local selected_instance
        selected_instance=$(echo "$instances" | fzf --header="Select an EC2 Instance" --height 40% --border)

        if [ -n "$selected_instance" ]; then
            local instance_id=$(echo $selected_instance | awk '{print $1}')
            local public_ip=$(echo $selected_instance | awk -F', Private IP: ' '{print $1}' | awk -F'Public IP: ' '{print $2}')
            local private_ip=$(echo $selected_instance | awk -F', Private IP: ' '{print $2}')
            echo "Selected instance: $selected_instance"
            echo "Public IP: $public_ip"
            echo "Private IP: $private_ip"
            echo "Instance ID: $instance_id"

            echo "Enter username for SSH connection: "
            read user
            # SSH using EC2 Instance Connect
            aws ec2-instance-connect ssh --os-user $user --instance-id $instance_id --connection-type eice
        else
            echo "No instance selected."
        fi
    fi
}
