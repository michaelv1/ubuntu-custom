import re

def parse_environment_string(env, environment_string):
    pattern = re.compile(rf'{re.escape(env)}-(\S+)')
    match = pattern.search(environment_string)
    
    if match:
        service_name = match.group(1)
        return {'env': env, 'service_name': service_name}
    else:
        # Handle invalid input or unexpected format
        return None

# Example usage:
env_value = 'ac-dev'
env_string = 'ac-dev-react-luminous-tt react-luminous-tt-2.9.2-4754-2108-v1553-ac-dev'

result = parse_environment_string(env_value, env_string)
if result:
    print(f"Env: {result['env']}")
    print(f"Service Name: {result.get('service_name', 'N/A')}")
    print()
