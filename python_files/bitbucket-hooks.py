import requests
import csv
import configparser
import os

# Configuration file path
credentials_file = os.path.expanduser('~/.bitbucket/michaelv-api-key.txt')

# Read credentials from file
config = configparser.ConfigParser()
config.read(credentials_file)

username = config.get('DEFAULT', 'username')
password = config.get('DEFAULT', 'password')

# Team configuration
team = 'anyclip'

# Initialize variables
full_repo_list = []
next_page_url = f'https://api.bitbucket.org/2.0/repositories/{team}?pagelen=100&fields=next,values'

# Fetch repositories for the given team
while next_page_url is not None:
    response = requests.get(next_page_url, auth=(username, password))

    # Check response status
    if response.status_code != 200:
        print(f"Failed to fetch repositories. Status Code: {response.status_code}, Response: {response.text}")
        break

    page_json = response.json()

    # Parse repositories from the JSON
    if 'values' in page_json:
        for repo in page_json['values']:
            full_repo_list.append(repo)
    else:
        break

    # Get the next page URL, if present
    next_page_url = page_json.get('next', None)

# Function to fetch webhooks for a repository
def fetch_webhooks(team, repo_slug, username, password):
    hooks_url = f'https://api.bitbucket.org/2.0/repositories/{team}/{repo_slug}/hooks'
    response = requests.get(hooks_url, auth=(username, password))

    if response.status_code == 200:
        return response.json().get('values', [])
    else:
        return []

# File to save the CSV
csv_file = 'webhooks.csv'

# Write CSV headers
with open(csv_file, mode='w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(['project_name', 'repo_name', 'webhook'])

    # Process repositories and write to CSV
    for repo in full_repo_list:
        project_name = repo['project']['name']
        repo_name = repo['name']
        repo_slug = repo['slug']

        webhooks = fetch_webhooks(team, repo_slug, username, password)

        if webhooks:
            for hook in webhooks:
                if 'jenkins' in hook['url'].lower():
                    writer.writerow([project_name, repo_name, hook['url']])

print(f"CSV file '{csv_file}' with webhooks containing 'jenkins' has been created.")
