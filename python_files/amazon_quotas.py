import boto3

serviceQuotaClinet = boto3.client('service-quotas')
response = serviceQuotaClinet.list_services()
results = response['Services']
while "NextToken" in response:
  response = serviceQuotaClinet.list_services(NextToken=response["NextToken"])
  results.extend(response["Services"])
for Service in results:
  detailedResponse = serviceQuotaClinet.list_service_quotas(ServiceCode=Service['ServiceCode'])
  detailedResults = detailedResponse['Quotas']
  while "NextToken" in detailedResponse:
    detailedResponse = serviceQuotaClinet.list_service_quotas(ServiceCode=Service['ServiceCode'],NextToken=detailedResponse["NextToken"])
    detailedResults.extend(detailedResponse["Quotas"])
  for Quota in detailedResults:
    print (Quota['ServiceCode'],":::",Quota['ServiceName'],":::",Quota['QuotaName'],":::",Quota['Value'])