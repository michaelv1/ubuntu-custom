#!/bin/bash

# Define the S3 bucket
S3_BUCKET="anyclip-pcn-production"

# Check if the number of arguments is valid
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <date_and_time> <worker>"
    exit 1
fi

# Parse the arguments
SPECIFIC_DATE="$1"
WORKER="$2"

# Create a folder named "downloaded" if it doesn't exist
DOWNLOAD_FOLDER="downloaded"
mkdir -p "$DOWNLOAD_FOLDER"

# Define the list of relevant prefixes
PREFIXES=(
    "events/lre/widget-aggregated-ads/"
    "events/lre/widget-aggregated-err-aer/"
    "events/lre/widget-aggregated-chips/"
    "events/lre/widget-aggregated-clicks/"
    "events/lre/widget-clips-aggregated/"
    "events/lre/widget-aggregated-comments/"
    "events/lre/widget-aggregated-csr/"
    "events/lre/widget-aggregated-forms/"
    "events/lre/widget-aggregated-live/"
    "events/lre/widget-aggregated-lux/"
    "events/lre/widget-aggregated-search/"
    "events/lre/widget-session-clips-aggregated/"
    "events/lre/widget-aggregated-targeting/"
    "events/lre/widget-aggregated-user/"
    "events/lre/widget-aggregated-xmodel/"
)

# Iterate through each prefix and download the files into the "downloaded" folder
for PREFIX in "${PREFIXES[@]}"; do
    LOCATION="${PREFIX}${SPECIFIC_DATE}/${WORKER}/"
    echo "Downloading files from location s3://${S3_BUCKET}/${LOCATION}:"
    aws s3 cp "s3://${S3_BUCKET}/${LOCATION}" "$DOWNLOAD_FOLDER" --recursive
    echo
done

