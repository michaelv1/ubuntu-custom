#!/bin/bash
#
# Make sure your context is configured to production cluster
# Edit the NGINX THE_DATE, and DURATION before running the script


bold=$(tput bold)
normal=$(tput sgr0)


#TAG='lre-backfill'
NGINX=8
THE_DATE='2024/01/30 00:00:00'
DURATION=1
#CONTAINER_VERSION=`kubectl get cronjobs.batch lre-events-aggregator-cronjob -o yaml | grep "image: 056612754658.dkr.ecr.us-east-1.amazonaws.com/lre-events-aggregator" | awk -F ":" '{print $3}'`

YEAR=`date -d "$THE_DATE" +%Y`
YEAR_SHORT=`date -d "$THE_DATE" +%y`
MONTH=`date -d "$THE_DATE" +%m`
MONTH_NO_LEAD_0=`date -d "$THE_DATE" +%-m`
DAY=`date -d "$THE_DATE" +%d`
DAY_NO_LEAD_0=`date -d "$THE_DATE" +%-d`
HOUR=`date -d "$THE_DATE" +%H`
HOUR_NO_LEAD_0=`date -d "$THE_DATE" +%-H`

echo $MONTH $MONTH_NO_LEAD_0
echo $DAY $DAY_NO_LEAD_0
echo $HOUR $HOUR_NO_LEAD_0
echo "YEAR_SHORT" , $YEAR_SHORT

WORKERS=`kubectl get pod | grep lre-events | grep -v aggregator | grep -v manager | wc -l`

echo "Check all files already exists in S3"

echo "aws s3 ls s3://anyclip-pcn-production/events/lre/widget-archive/${YEAR}/${MONTH}/${DAY}/${HOUR}/"
echo "aws s3 ls s3://anyclip-pcn-production/events/lre/widget-temp/${YEAR}/${MONTH}/${DAY}/${HOUR}/"

widgetarchive=`aws s3 ls s3://anyclip-pcn-production/events/lre/widget-archive/${YEAR}/${MONTH}/${DAY}/${HOUR}/ | wc -l`
widgettemp=`aws s3 ls s3://anyclip-pcn-production/events/lre/widget-temp/${YEAR}/${MONTH}/${DAY}/${HOUR}/ | wc -l`
ALL_FILES=`expr $NGINX \* 60`
EXISTS_FILES=`echo $widgetarchive + $widgettemp | bc -l`
echo "ALL_FILES:" $ALL_FILES
echo "EXISTS_FILES:" $EXISTS_FILES

echo ""
echo "For Full BAckup Proccess scale backfill deployment"
echo ""
echo $bold"kubectl scale deployment lre-events-aggregator-backfill --replicas 1"$normal
echo "------------------------------"
echo "in the container run screen and inside run the copy files and the backfill proccess" 
echo ""
echo $bold"screen"

echo "## This should be run inside a container" > copy_files_to_backfill.sh
workers="worker-0 worker-1 worker-2 worker-3 worker-4 worker-5 worker-6 worker-7 worker-8 worker-9 worker-10 worker-11 worker-12 worker-13"
for worker in $workers;
do
  echo "aws s3 cp s3://anyclip-pcn-production/events/lre/widget-archive/${YEAR}/${MONTH}/${DAY}/${HOUR}/$worker s3://anyclip-pcn-production/events/lre/widget-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/ --recursive" >> copy_files_to_backfill.sh
  echo "aws s3 cp s3://anyclip-pcn-production/events/lre/widget-temp/${YEAR}/${MONTH}/${DAY}/${HOUR}/$worker s3://anyclip-pcn-production/events/lre/widget-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/ --recursive" >> copy_files_to_backfill.sh
  echo "aws s3 cp s3://anyclip-pcn-production/events/lre/widget-managed/${YEAR}/${MONTH}/${DAY}/${HOUR}/$worker s3://anyclip-pcn-production/events/lre/widget-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/ --recursive" >> copy_files_to_backfill.sh
done
echo "aws s3 ls s3://anyclip-pcn-production/events/lre/widget-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/ | wc -l" >> copy_files_to_backfill.sh

echo "python backfill.py --year ${YEAR} --month ${MONTH} --day ${DAY} --hour ${HOUR} --duration ${DURATION}"$normal
echo "-------------------------------"

echo "In case that all the files was loaded but res.txt.zip is not creted you need to run merge"
echo ""
echo $bold"kubectl scale deployment lre-events-aggregator-merge --replicas 1"$normal
echo "-------------------------------"
echo "in the container run the merge command"
echo ""
echo $bold"python lre_events.py --merge_backfill_back_date_hour ${DAY}-${MONTH}-${YEAR_SHORT}-${HOUR}"$normal
echo "-------------------------------"

echo "python backfill.py --year ${YEAR} --month ${MONTH} --day ${DAY} --hour ${HOUR} --duration ${DURATION}" > run_backfill.sh

chmod +x copy_files_to_backfill.sh run_backfill.sh

echo "Check if zip file existsi and only need to upload to redshift"
echo ""
echo $bold"python rs_update.py --table_name pcn.pcn_clip_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-clips-aggregated/${YEAR}/${MONTH_NO_LEAD_0}/${DAY_NO_LEAD_0}/${HOUR_NO_LEAD_0}/clips.txt.zip --replace False --table_definition clips_table.sql --copy_command clips_copy.sql"
echo "python rs_update.py --table_name pcn.pcn_xmodel_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-aggregated-xmodel/${YEAR}/${MONTH_NO_LEAD_0}/${DAY_NO_LEAD_0}/${HOUR_NO_LEAD_0}/xmodel.txt.zip --replace False --table_definition xmodel_table.sql --copy_command xmodel_copy.sql"
echo "python rs_update.py --table_name pcn.pcn_lux_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-aggregated-lux/${YEAR}/${MONTH_NO_LEAD_0}/${DAY_NO_LEAD_0}/${HOUR_NO_LEAD_0}/lux.txt.zip --replace False --table_definition lux_table.sql --copy_command lux_copy.sql"$normal
echo "-------------------------------"
echo "When backfill is finished and no data loaded check iffiles exists and run"
echo ""
echo $bold"python rs_update.py --table_name pcn.pcn_clip_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-clips-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/clips.txt.zip --replace False --table_definition clips_table.sql --copy_command clips_copy.sql"
echo "python rs_update.py --table_name pcn.pcn_xmodel_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-aggregated-xmodel-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/xmodel.txt.zip --replace False --table_definition xmodel_table.sql --copy_command xmodel_copy.sql"
echo "python rs_update.py --table_name pcn.pcn_lux_aggregated --source_path s3://anyclip-pcn-production/events/lre/widget-aggregated-lux-backfill/${YEAR}/${MONTH}/${DAY}/${HOUR}/lux.txt.zip --replace False --table_definition lux_table.sql --copy_command lux_copy.sql"$normal
echo "------------------------------------------"

