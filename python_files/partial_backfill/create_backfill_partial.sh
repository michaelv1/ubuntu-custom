#!/bin/bash
#
# Make sure your context is configured to production cluster
# Edit the NGINX THE_DATE, and DURATION before running the script


bold=$(tput bold)
normal=$(tput sgr0)


#Set the date and missing worker'
THE_DATE='2024/01/31 15:00:00'
WORKER=worker-11

YEAR=`date -d "$THE_DATE" +%Y`
YEAR_SHORT=`date -d "$THE_DATE" +%y`
MONTH=`date -d "$THE_DATE" +%m`
MONTH_NO_LEAD_0=`date -d "$THE_DATE" +%-m`
DAY=`date -d "$THE_DATE" +%d`
DAY_NO_LEAD_0=`date -d "$THE_DATE" +%-d`
HOUR=`date -d "$THE_DATE" +%H`
HOUR_NO_LEAD_0=`date -d "$THE_DATE" +%-H`

echo "date ${YEAR}/${MONTH}/${DAY}/${HOUR}"
echo "missing-worker: ${WORKER}"

echo ""
echo "scale the deployment for backfill-partial"
echo ""
echo $bold"kubectl scale deployment lre-events-aggregator-backfill-partial --replicas 1"
echo "CONTAINER=\`kgp | grep backfill-partial | awk '{print \$1}'\`"$normal
echo "wait until the container is running"
echo $bold"kgpw | grep \${CONTAINER}"
echo "kubectl cp copy_files.py \${CONTAINER}:/usr/src/app/copy_files.py"$mormal
echo "keti \${CONTAINER} -- python /usr/src/app/copy_files.py ${YEAR}/${MONTH}/${DAY}/${HOUR} ${WORKER}"
echo "keti \${CONTAINER} -- python backfill.py --year ${YEAR} --month ${MONTH} --day ${DAY} --hour ${HOUR} --duration 1 --nginx_prefix ${WORKER}"
echo "keti \${CONTAINER} -- python lre_events.py --merge_backfill_back_date_hour ${YEAR}-${DAY}-${MONTH}-${HOUR}"$normal
echo "------------------------------"

