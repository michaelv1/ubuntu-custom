#!/bin/bash

# Define the S3 bucket
S3_BUCKET="anyclip-pcn-production"

# Check if the number of arguments is valid
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <date_and_time> <worker>"
    exit 1
fi

# Parse the arguments
SPECIFIC_DATE="$1"
WORKER="$2"

# Define the list of relevant prefixes
PREFIXES=(
    "events/lre/widget-aggregated-ads/"
    "events/lre/widget-aggregated-err-aer/"
    "events/lre/widget-aggregated-chips/"
    "events/lre/widget-aggregated-clicks/"
    "events/lre/widget-clips-aggregated/"
    "events/lre/widget-aggregated-comments/"
    "events/lre/widget-aggregated-csr/"
    "events/lre/widget-aggregated-forms/"
    "events/lre/widget-aggregated-live/"
    "events/lre/widget-aggregated-lux/"
    "events/lre/widget-aggregated-search/"
    "events/lre/widget-session-clips-aggregated/"
    "events/lre/widget-aggregated-targeting/"
    "events/lre/widget-aggregated-user/"
    "events/lre/widget-aggregated-xmodel/"
)

# Iterate through each prefix and print the files inside each location
for PREFIX in "${PREFIXES[@]}"; do
    LOCATION="${PREFIX}${SPECIFIC_DATE}/${WORKER}/"
    #echo "Files in location s3://${S3_BUCKET}/${LOCATION}:"
    aws s3 ls "s3://${S3_BUCKET}/${LOCATION}"
    #echo
done

