import boto3
import sys

def copy_files(source_bucket, source_prefix, destination_bucket, destination_prefix):
    s3 = boto3.client('s3')
    files_copied = 0  # Initialize counter for files copied
    
    # List all objects in the source prefix
    response = s3.list_objects_v2(Bucket=source_bucket, Prefix=source_prefix)
    if 'Contents' in response:
        for obj in response['Contents']:
            source_key = obj['Key']
            # Remove the source prefix from the key to get the relative path
            relative_key = source_key[len(source_prefix):]
            # Construct the destination key
            destination_key = destination_prefix + relative_key
            # Copy the object
            copy_source = {'Bucket': source_bucket, 'Key': source_key}
            s3.copy_object(CopySource=copy_source, Bucket=destination_bucket, Key=destination_key)
            files_copied += 1  # Increment counter for each file copied
            # Log the files that are copied to the screen
            print(f"Copied: {source_key} to {destination_key}")

    return files_copied  # Return the total number of files copied

def main(date, worker):
    # Source and destination bucket
    source_bucket = 'anyclip-pcn-production'
    destination_bucket = 'anyclip-pcn-production'
    
    # Source and destination prefixes
    source_prefixes = [
        'events/lre/widget-archive/{}/{}/'.format(date, worker),
        'events/lre/widget-temp/{}/{}/'.format(date, worker)
    ]
    destination_prefix = 'events/lre/widget-backfill/{}/{}/'.format(date, worker)
    
    total_files_copied = 0  # Initialize total files copied counter
    
    for source_prefix in source_prefixes:
        total_files_copied += copy_files(source_bucket, source_prefix, destination_bucket, destination_prefix)
    
    print(f"Total files copied: {total_files_copied}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py <date> <worker>")
        sys.exit(1)
        
    date = sys.argv[1]
    worker = sys.argv[2]
    main(date, worker)

