import imaplib
import base64
import os
import email
import sys
import zipfile
from sh import gunzip
import xml.etree.ElementTree as ET
from pathlib import Path

home = str(Path.home())


email_user = "michaelv@anyclip.com"
email_pass = "Quer9@451@"

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.login(email_user, email_pass)
mail.select('inbox')

m_type, data = mail.search(None, '(SUBJECT "Report Domain")')

for num in data[0].split():
    rv, data = mail.fetch(num, '(RFC822)')
    msg = email.message_from_bytes(data[0][1])
    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            # print part.as_string()
            continue
        if part.get('Content-Disposition') is None:
            # print part.as_string()
            continue
        fileName = part.get_filename()

        if bool(fileName):
            filePath = os.path.join(home+'/Downloads/attachment', fileName)
            if not os.path.isfile(filePath) :
                fp = open(filePath, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()
            mail.store(num, '+X-GM-LABELS', 'automated-emails-dmarc')
            mail.store(num, '+FLAGS', '\\Deleted')

for f in os.listdir(home+'/Downloads/attachment'):
    if f.endswith(".zip"):
        os.chdir(home+'/Downloads/attachment')
        z = zipfile.ZipFile(f, 'r')
        z.extractall(path=os.path.dirname(f))
        z.close()
        os.remove(f)

for f in os.listdir(home+'/Downloads/attachment'):
    if f.endswith(".gz"):
        os.chdir(home+'/Downloads/attachment')
        gunzip(f)

for f in os.listdir(home+'/Downloads/attachment'):
    print (f)
    os.chdir(home+'/Downloads/attachment')
    with open(f) as read_file:
        if 'fail' not in read_file.read():
            print ("file does not contain fail: " + f )
            os.remove(f)

for f in os.listdir(home+'/Downloads/attachment'):
    #print (f)
    os.chdir(home+'/Downloads/attachment')
    tree = ET.parse(f)
    root = tree.getroot()
    for record in root.findall('record'):
        for row in record.findall('row'):
            for source_ip in row.findall('source_ip'):
                SOURCE_IP = source_ip.text
            for policy_evaluated in row.findall('policy_evaluated'):
                for dkim in policy_evaluated.findall('dkim'):
                    if dkim.text == 'fail':
                        print (SOURCE_IP, dkim.text)
                for spf in policy_evaluated.findall('spf'):
                    if spf.text == 'fail':
                        print (SOURCE_IP, spf.text)

        for auth_results in record.findall('auth_results'):
            for spf in auth_results.findall('spf'):
                for domain in spf.findall('domain'):
                    DOMAIN = domain.text
                for result in spf.findall('result'):
                    if result.text != 'pass':
                        print (DOMAIN, result.text)

        for results in record.findall('auth_results'):
            for spf in results.findall('spf'):
                for result in spf.findall('result'):
                    if result.text == 'fail':
                        print (result.text)

        for results in record.findall('auth_results'):
            for dkim in results.findall('dkim'):
                for result in dkim.findall('result'):
                    if result.text == 'fail':
                        print (result.text)


