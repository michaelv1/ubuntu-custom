import subprocess
import re

bashCommand = "helm ls | awk '{print ($1,\",\"$9)}' | sed s/^ac-prod-//g | sed s/-ac-prod$//g"
process = subprocess.Popen([bashCommand],shell=True, universal_newlines = True, stdout=subprocess.PIPE).stdout.read()
for line in process.split(' '):
  print (line)