import requests
import configparser
import os
import sys

def fetch_bitbucket_repos(team, username, password):
  """
  Fetch all repositories for the given team from Bitbucket's API.
  Returns a list of repository JSON objects.
  """
  base_url = f"https://api.bitbucket.org/2.0/repositories/{team}"
  page_len = 100
  fields = "next,values"  # Minimizes payload to only the needed fields

  all_repos = []
  next_page_url = f"{base_url}?pagelen={page_len}&fields={fields}"

  while next_page_url:
    response = requests.get(next_page_url, auth=(username, password))

    if response.status_code != 200:
      print(
        f"Failed to fetch repositories.\n"
        f"Status Code: {response.status_code}\n"
        f"Response: {response.text}"
      )
      # You can decide how to handle partial data if an error occurs
      return all_repos  # or raise an exception

    data = response.json()
    all_repos.extend(data.get('values', []))

    # Get the next page if present
    next_page_url = data.get('next')

  return all_repos

def main():
  # Credentials file location
  credentials_file = "/home/michaelv/.bitbucket/michaelv-api-key.txt"

  # Check if credentials file exists
  if not os.path.exists(credentials_file):
    print(f"Error: Credentials file '{credentials_file}' not found.")
    sys.exit(1)

  # Read credentials using configparser
  config = configparser.ConfigParser()
  config.read(credentials_file)

  # Pull username and password from [DEFAULT] section
  username = config.get('DEFAULT', 'username')
  password = config.get('DEFAULT', 'password')

  # Define the team (workspace) to fetch from
  team = "anyclip"

  # Fetch all repositories
  full_repo_list = fetch_bitbucket_repos(team, username, password)
  if not full_repo_list:
    print("No repositories found or an error occurred while fetching.")
    sys.exit(1)

  # Prepare to write the two scripts
  devops_config_count = 0
  devops_other_count = 0

  with open('anyclip-devops.sh', 'w') as f_config:
    # We assume the user will want to clone into ~/git
    print('cd ~/git', file=f_config)
    for repo in full_repo_list:
      project_name = repo.get('project', {}).get('name', '')
      repo_name = repo.get('name', '')
      if project_name == 'AnyClip-DevOps' and 'devops-config' in repo_name:
        clone_line = f"git clone git@bitbucket.org:anyclip/{repo_name}.git"
        print(clone_line, file=f_config)
        devops_config_count += 1

  with open('anyclip-devops-unique.sh', 'w') as f_unique:
    print('cd ~/git', file=f_unique)
    for repo in full_repo_list:
      project_name = repo.get('project', {}).get('name', '')
      repo_name = repo.get('name', '')
      if project_name == 'AnyClip-DevOps' and 'devops-config' not in repo_name:
        clone_line = f"git clone git@bitbucket.org:anyclip/{repo_name}.git"
        print(clone_line, file=f_unique)
        devops_other_count += 1

  # Print the counts of repositories in each script
  print(f"anyclip-devops.sh: {devops_config_count} repos")
  print(f"anyclip-devops-unique.sh: {devops_other_count} repos")

if __name__ == "__main__":
  main()
